
class UnknownNumberTypeException(Exception):
    pass


class WrongArgumentException(Exception):
    pass


class DivisorIsZeroException(Exception):
    pass

class ShouldntBeHereException(Exception):
    pass

class ComparisonException(Exception):
    pass